package com.nostratech.example.app.controller;

import com.nostratech.example.app.exception.BackendExampleException;
import com.nostratech.example.app.util.RestUtil;
import com.nostratech.example.app.vo.ResultPageVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

/**
 * Created by yukibuwana on 1/24/17.
 */

public abstract class AbstractRequestPageHandler {

    private static final Logger log = LoggerFactory.getLogger(AbstractRequestPageHandler.class);

    public ResponseEntity<ResultPageVO> getResult() {
        ResultPageVO result = new ResultPageVO();
        try {
            return processRequest();
        } catch (BackendExampleException e) {
            result.setMessage(e.getCode().name());
            result.setResult(null);
            result.setError(e.getMessage());

            log.error("ERROR", e);
        }
        return RestUtil.getJsonResponse(result);
    }

    public abstract ResponseEntity<ResultPageVO> processRequest();
}
