package com.nostratech.example.app.persistence.repository;

import com.nostratech.example.app.persistence.domain.People;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yukibuwana on 1/30/17.
 */

@Repository
public interface PeopleRepository extends BaseRepository<People> {


    People findByName(String name);
    @Query(value="SELECT DISTINCT p.address FROM People p")
    List<String> findAddressDistinct();
    List<People> findByAddress(String address);


}
