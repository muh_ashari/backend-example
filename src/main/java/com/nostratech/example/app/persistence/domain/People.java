package com.nostratech.example.app.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Created by yukibuwana on 1/29/17.
 */

@Entity
@Table(name = "PEOPLE",
        indexes = {
                @Index(columnList = "NAME", name = "UK_NAME", unique = true)
        })
@DynamicUpdate
@Data
public class People extends Base {

    @Column(name = "NAME", length = 20, nullable = false)
    private String name;

    @Column(name = "ADDRESS", length = 50, nullable = false)
    private String address;

    public People() { }

    public People(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
