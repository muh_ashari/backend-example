package com.nostratech.example.app.persistence.repository;

import com.nostratech.example.app.persistence.domain.Adminduk;
import com.nostratech.example.app.persistence.domain.Base;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.vo.BaseVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdmindukRepository extends BaseRepository<Adminduk> {

    @Query(value="SELECT DISTINCT a._id FROM Adminduk a")
    List<String> find_idDistinct();
    List<Adminduk> findBy_id(String _id);

    @Query(value = "SELECT a.secure_id FROM Adminduk a WHERE _id= :_id",nativeQuery = true)
    String get_id(@Param("_id") String _id);

    @Query(value = "SELECT a.version FROM Adminduk a WHERE _id= :_id",nativeQuery = true)
    int get_version(@Param("_id") String _id);
}