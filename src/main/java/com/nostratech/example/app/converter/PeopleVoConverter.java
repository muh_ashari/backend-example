package com.nostratech.example.app.converter;

import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.util.ExtendedSpringBeanUtil;
import com.nostratech.example.app.vo.PeopleRequestVO;
import com.nostratech.example.app.vo.PeopleResponseVO;
import org.springframework.stereotype.Component;

/**
 * Created by yukibuwana on 1/24/17.
 */

@Component
public class PeopleVoConverter extends BaseVoConverter<PeopleRequestVO, PeopleResponseVO, People> implements
        IBaseVoConverter<PeopleRequestVO, PeopleResponseVO, People> {

    @Override
    public PeopleResponseVO transferModelToVO(People model, PeopleResponseVO vo) {
        if (null == vo) vo = new PeopleResponseVO();
        super.transferModelToVO(model, vo);

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo, new String[]{"name", "address"});

        return vo;
    }

    @Override
    public People transferVOToModel(PeopleRequestVO vo, People model) {
        if (null == model) model = new People();
        super.transferVOToModel(vo, model);

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model, new String[]{"name", "address"});

        return model;
    }
}
