package com.nostratech.example.app.batch;

import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

/**
 * Created by yukibuwana on 2/1/17.
 */

@Configuration
@EnableBatchProcessing
public class PeopleBatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    PeopleRepository peopleRepository;

    @Bean
    public FlatFileItemReader<People> reader() {
        FlatFileItemReader<People> reader = new FlatFileItemReader<>();
        reader.setResource(new ClassPathResource("data/people.csv"));
        reader.setLinesToSkip(1);
        reader.setLineMapper(new DefaultLineMapper<People>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "name", "address" });
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<People>() {{
                setTargetType(People.class);
            }});
        }});
        return reader;
    }

    @Bean
    public PeopleItemProcessor processor() {
        return new PeopleItemProcessor();
    }

    @Bean
    public RepositoryItemWriter<People> writer() {
        RepositoryItemWriter<People> writer = new RepositoryItemWriter<>();
        writer.setRepository(peopleRepository);
        writer.setMethodName("save");

        return writer;
    }

    @Bean
    public Job importPeopleJob(PeopleNotificationListener listener) {
        return jobBuilderFactory.get("importPeopleJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<People, People> chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }
}
