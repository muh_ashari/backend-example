package com.nostratech.example.app.batch;

import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by yukibuwana on 2/1/17.
 */
public class PeopleItemProcessor implements ItemProcessor<People, People> {

    private static final Logger log = LoggerFactory.getLogger(PeopleItemProcessor.class);

    @Autowired
    PeopleRepository peopleRepository;

    @Override
    public People process(People people) throws Exception {
        final String name = people.getName();
        final String address = people.getAddress();

        People find = peopleRepository.findByName(name);

        if (find != null) {
            log.info(String.format("%s - %S already found", name, address));

            return null;
        } else {
            final People transformedPeople = new People(name, address);

            return transformedPeople;
        }
    }
}
