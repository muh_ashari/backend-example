package com.nostratech.example.app.vo;

import lombok.Data;
/**
 * Created by yukibuwana on 1/24/17.
 */

@Data
public class ResultVO extends BaseVO {

    private String message;
    private Object result;
    private String error;

    public ResultVO() {
    }

    public ResultVO(String message, Object result, String error) {
        this.message = message;
        this.result = result;
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
