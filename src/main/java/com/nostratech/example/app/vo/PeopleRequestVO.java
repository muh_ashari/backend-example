package com.nostratech.example.app.vo;

import lombok.Data;

/**
 * Created by yukibuwana on 1/30/17.
 */

@Data
public class PeopleRequestVO extends BaseVO {

    String name;
    String address;
}
