package com.nostratech.example.app.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by yukibuwana on 1/30/17.
 */

@Data
public class AdmindukListVO extends BaseVO {

    List<AdmindukResponseVO> listAdminduk;
}
