package com.nostratech.example.app.exception;

import com.nostratech.example.app.enums.StatusCode;
import lombok.Data;

/**
 * Created by yukibuwana on 1/24/17.
 */

@Data
public class BackendExampleException extends RuntimeException {

    private StatusCode code = StatusCode.ERROR;

    public BackendExampleException() {
        super();
    }

    public BackendExampleException(String message) {
        super(message);
    }

    public BackendExampleException(String message, StatusCode code) {
        super(message);
        this.code = code;
    }

    public StatusCode getCode() {
        return code;
    }

    public void setCode(StatusCode code) {
        this.code = code;
    }
}
