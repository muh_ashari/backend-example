package com.nostratech.example.app.exception;

import com.nostratech.example.app.enums.StatusCode;
import com.nostratech.example.app.vo.ResultVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by yukibuwana on 1/25/17.
 */

@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    @ExceptionHandler(value = {Exception.class, RuntimeException.class })
    public ResponseEntity<?> defaultErrorHandler(HttpServletRequest req, Exception e) {

        log.error(String.format("%s : Caught in Global Exception Handler for req: %s",
                e.getLocalizedMessage(), req.getRequestURL()));
        log.error("GlobalDefaultExceptionHandler: ", e);

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        ResultVO restResponseVO = new ResultVO();
        restResponseVO.setMessage(StatusCode.ERROR.name());
        restResponseVO.setResult(null);
        restResponseVO.setError(e.getMessage());

        return new ResponseEntity<>(restResponseVO, status);
    }
}
