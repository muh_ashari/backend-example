package com.nostratech.example.app.service;

import oracle.stellent.ridc.IdcClient;
import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.IdcClientManager;
import oracle.stellent.ridc.IdcContext;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.TransferFile;
import oracle.stellent.ridc.protocol.ServiceResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Collection;

/**
 * Created by yukibuwana on 1/24/17.
 */
@Service
public class ConnectIntoWCCService {

    @Value("${wcc.hostname}")
    private String host;
    @Value("${wcc.hostname2}")
    private String host2;
    @Value("${wcc.port}")
    private String port;
    @Value("${wcc.user}")
    private String user;
    @Value("${wcc.pass}")
    private String pass;

    private  IdcClientManager myIdcClientManager;
    private  IdcClient myIdcClient;
    private  IdcContext myIdcContext;

    public String uploadFile(String namepathFile, String titleFile, String nip, String docType) throws IdcClientException, IOException
    {
        myIdcClientManager = new IdcClientManager();
        System.out.println(myIdcClientManager.toString());
        myIdcClient = myIdcClientManager.createClient("idc://"+host+":"+port);
        System.out.println(myIdcClient.toString());
        myIdcContext = new IdcContext(user);
        System.out.println(myIdcContext.toString());
        InputStream is = new FileInputStream(namepathFile);
        System.out.println(is.toString());
        myIdcClient.getConfig().setSocketTimeout(30000);
        myIdcClient.getConfig().setConnectionSize(20);

        DataBinder binder = myIdcClient.createBinder();
        binder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
        binder.putLocal("dDocTitle", titleFile);
        binder.putLocal("dDocType", "Document");
        //binder.putLocal("dDocType", "DigitalMedia");
        binder.putLocal("dSecurityGroup", "Public");
        binder.putLocal("xxNamaDokumen", titleFile);
        binder.putLocal("xxTipeDokumen", docType);
//        binder.putLocal("xxProfileTrigger", "Controlled Document"); <- ga dipake di api, dipakenya kalo akses langsung
//        binder.putLocal("xxKedeputian", deputy);
        binder.putLocal("xxNip", nip);
//        binder.addFile("primaryFile", new TransferFile(new File(namepathFile)));
        binder.addFile("primaryFile", new TransferFile(is,namepathFile,new File(namepathFile).length(),"text/html"));

        ServiceResponse response = myIdcClient.sendRequest(myIdcContext, binder);
        System.out.println(response.toString());
        DataBinder binderResult = response.getResponseAsBinder();

        //to get dID so that user will get document with the correct name file
        String dID=binderResult.getLocal("dID");
//        System.out.println(test);

        String docUrl = binderResult.getLocal("WebfilePath");
        System.out.println(docUrl);
        int idx = docUrl.lastIndexOf("groups/");
        System.out.println(idx);
        String fullpath = docUrl.substring(idx, docUrl.length());
        idx = fullpath.lastIndexOf("~");
        fullpath = fullpath.substring(0,idx) + "." + docUrl.split("[.]")[1];

        if (titleFile.toLowerCase().contains("docx") || titleFile.toLowerCase().contains("doc")){
            return "http://"+host+"/cs/idcplg?IdcService=GET_FILE&dID="+dID;
        }else{
            return "http://"+host+"/cs/"+fullpath;
        }

//        return "http://"+host+":16200/cs/"+fullpath;
//        return "http://"+host2+"/cs/"+fullpath;
//        return "http://"+host2+"/cs/idcplg?IdcService=GET_FILE&dID="+dID+",,,,,"+"http://"+host2+"/cs/"+fullpath;
        //return "http://wc1:16200/cs/"+fullpath;
    }
}
