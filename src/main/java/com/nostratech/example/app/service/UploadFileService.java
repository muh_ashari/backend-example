package com.nostratech.example.app.service;


import com.nostratech.example.app.exception.BackendExampleException;
import com.nostratech.example.app.vo.HTMLToDocVO;
import com.nostratech.example.app.vo.SingleFileVO;
import oracle.stellent.ridc.IdcClientException;
import org.docx4j.Docx4J;
import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.convert.out.HTMLSettings;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.Parts;
import org.docx4j.openpackaging.parts.SpreadsheetML.SharedStrings;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.xlsx4j.jaxb.Context;
import org.xlsx4j.sml.*;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.URLConnection;
import java.util.Base64;

/**
 * Created by yukibuwana on 1/24/17.
 */
@Service
public class UploadFileService{

    private static final Logger log = LoggerFactory.getLogger(UploadFileService.class);

    @Autowired
    ConnectIntoWCCService intoWCC;

    public String parseFile(SingleFileVO paramBase64)
    {
        Base64.Decoder decode = Base64.getDecoder();

        if(paramBase64.getStrBase64().toLowerCase().contains("data:"))
        {
            byte[] decodeArray = decode.decode(paramBase64.getStrBase64().split("[,]")[1]);
            String created = createFile(decodeArray, typeFile(paramBase64), paramBase64.getNameFile(), paramBase64.getNip(), paramBase64.getDocType());
            if(created != null) return created;
            else throw new BackendExampleException("Error parse file, please checkedPlatform your code base64");
        }

        else
        {

            byte[] decodeArray = decode.decode(paramBase64.getStrBase64());
            String created = createFile(decodeArray, paramBase64.getNameFile().split("[.]")[1], paramBase64.getNameFile().split("[.]")[0], paramBase64.getNip(), paramBase64.getDocType());
            if(created != null) return created;
            else throw new BackendExampleException("Error parse file, please checkedPlatform your code base64");
        }
    }

    private String typeFile(SingleFileVO fileVO)
    {
        String[] parts = fileVO.getStrBase64().split("[,]");
        String imagestring = parts[1];
        Base64.Decoder decode = Base64.getDecoder();
        byte[] imagearray = decode.decode(imagestring);

        InputStream is = new ByteArrayInputStream(imagearray);

        try
        {
            String mimetype = URLConnection.guessContentTypeFromStream(is);
            String[] tokens = mimetype.split("[/]");
            return tokens[1];
        }

        catch(Exception er)
        {
            throw new BackendExampleException(er.getMessage());
        }
    }

    private String createFile(byte[] decode, String extensionFile, String nameFile, String nip, String docType)
    {
        String rtr = null;

        try
        {
//            String fullPath = "/home/oracle/temp/"+nameFile+"."+extensionFile;
            String fullPath = "C:/Users/Lenovo/Documents/Temp"+nameFile+"."+extensionFile;
            File file = new File(fullPath);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(decode);
            bos.flush();
            bos.close();

            if(new File(fullPath).exists()){
                rtr =  intoWCC.uploadFile(fullPath, nameFile, nip, docType);
                new File(fullPath).delete();
            }
        }

        catch (IOException e)
        {
            throw new BackendExampleException("IOException "+e.getMessage());
        }

        catch(IdcClientException e)
        {
            throw new BackendExampleException("IdcClientException "+e.getMessage());
        }

        return rtr;
    }

    public String createFile(MultipartFile data, String nameFile, String nip, String docType)
    {
        String rtr = null;
        try
        {

            //String fullPath = "/home/oracle/temp/"+data.getOriginalFilename();
            String fullPath = "C:/Users/Lenovo/Documents/Temp"+data.getOriginalFilename();
            File file = new File(fullPath);
            data.transferTo(file);

            if(new File(fullPath).exists()){
                rtr =  intoWCC.uploadFile(fullPath, nameFile, nip, docType);
                new File(fullPath).delete();
            }
        }

        catch (IOException e)
        {
            throw new BackendExampleException("IOException "+e.getMessage());
        }

        catch(IdcClientException e)
        {
            throw new BackendExampleException("IdcClientException "+e.getMessage());
        }

        return rtr;
    }


    //function for converting HTML script to Document file
    public String uploadHtmlToDoc(HTMLToDocVO htmlToDocVO) {
//        String destPath="/home/oracle/temp/"; //To upload to server path
        String destPath="C:/Users/Lenovo/Documents/Temp"; //To upload to local path

        //for creating temporary document file
        File dir = new File (destPath);
        File actualFile = new File (dir, htmlToDocVO.getFileName());
        String input=htmlToDocVO.getInput();

        //e.g /home/oracle/temp/test.docx
        String namePathFile = destPath+"/"+htmlToDocVO.getFileName();


        try {
            input = java.net.URLDecoder.decode(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //To remove <br> and add </img> so that the html script will be proper
        input = input.replaceAll("<br>","<br/>");
        input = input.replaceAll("<img(.+?)>", "<center><img$1></img></center>");
        input = input.replaceAll("width(.+?)\"", "width:30%;\"");
        System.out.println(input);




        //Converting html script into document
        WordprocessingMLPackage wordMLPackage = null;
        try {
            wordMLPackage = WordprocessingMLPackage.createPackage();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);
        try {
            wordMLPackage.getMainDocumentPart().getContent().addAll(
                    XHTMLImporter.convert( input, null) );
        } catch (Docx4JException e) {
            e.printStackTrace();
        }
        HTMLSettings htmlSettings = Docx4J.createHTMLSettings();
        htmlSettings.setWmlPackage(wordMLPackage);


        // output to an OutputStream.
//        OutputStream os = new ByteArrayOutputStream();
        // If you want XHTML output
//        Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML",
//                true);
        try {
            htmlSettings.setWmlPackage(wordMLPackage);
//            Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML",
//                    true);
//            Docx4J.toHTML(htmlSettings, os, Docx4J.FLAG_EXPORT_PREFER_XSL);
            wordMLPackage.save(actualFile);
        } catch (Docx4JException e) {
            e.printStackTrace();
        }

        //To upload the converted document to WCC
        String output="";
        if(new File(destPath).exists()){
            try {
                output =  intoWCC.uploadFile(namePathFile, htmlToDocVO.getFileName(), htmlToDocVO.getNip(), htmlToDocVO.getDocType());
//                output="done";
            } catch (IdcClientException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //To delete temporary document file so that it wont flood the server
//            new File(namePathFile).delete();
        }

        return output;
    }

    public String uploadHtmlToXls(HTMLToDocVO htmlToDocVO){

        String destPath="C:/Users/Lenovo/Documents/Temp"; //To upload to local path

        //for creating temporary document file
        File dir = new File (destPath);
        File actualFile = new File (dir, htmlToDocVO.getFileName());
        String input=htmlToDocVO.getInput();

        //e.g /home/oracle/temp/test.docx
        String namePathFile = destPath+"/"+htmlToDocVO.getFileName();


        try {
            input = java.net.URLDecoder.decode(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        SpreadsheetMLPackage pkg = null;
        try {
            pkg = SpreadsheetMLPackage.createPackage();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

// Create a new worksheet part and retrieve the sheet data
        WorksheetPart sheet = null;
        try {
            sheet = pkg.createWorksheetPart(new PartName("/xl/worksheets/sheet1.xml"), "Sheet 1", 1);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        SheetData sheetData = sheet.getJaxbElement().getSheetData();

// Keep track of how many strings we've added
        long sharedStringCounter = 0;

// Create a new row
        Row row = Context.getsmlObjectFactory().createRow();

// Create a shared strings table instance
        CTSst sharedStringTable = new CTSst();
        CTXstringWhitespace ctx;
        CTRst crt;

// Create 10 cells and add them to the row
        for (int i = 0; i < 10; i++) {

            // Create a shared string
            crt = new CTRst();
            ctx = Context.getsmlObjectFactory().createCTXstringWhitespace();
            ctx.setValue(input + Integer.toString(i + 1));
            crt.setT(ctx);

            // Add it to the shared string table
            sharedStringTable.getSi().add(crt);

            // Add a reference to the shared string to our cell using the counter
            Cell cell = Context.getsmlObjectFactory().createCell();
            cell.setT(STCellType.S);
            cell.setV(String.valueOf(sharedStringCounter));

            // Add the cell to the row and increment the counter
            row.getC().add(cell);
            sharedStringCounter++;
        }

// Add the row to our sheet
        sheetData.getRow().add(row);

// Set the string and unique string counts on the shared string table
        sharedStringTable.setCount(sharedStringCounter);
        sharedStringTable.setUniqueCount(sharedStringCounter);

// Create a SharedStrings workbook part
        SharedStrings sharedStrings = null;
        try {
            sharedStrings = new SharedStrings(new PartName("/xl/sharedStrings.xml"));
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

// Add the shared string table to the part
        sharedStrings.setJaxbElement(sharedStringTable);

// Then add the part to the workbook
        Parts parts = pkg.getParts();
        Part workBook = null;
        try {
            workBook = parts.get( new PartName("/xl/workbook.xml") );
            workBook.addTargetPart(sharedStrings);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        String output="";
        return output;

    }

}
