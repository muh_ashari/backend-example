package com.nostratech.example.app.service;

import com.nostratech.example.app.converter.IBaseVoConverter;
import com.nostratech.example.app.converter.PeopleVoConverter;
import com.nostratech.example.app.exception.BackendExampleException;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.BaseRepository;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import com.nostratech.example.app.validator.PeopleValidator;
import com.nostratech.example.app.vo.PeopleCustomResponseVO;
import com.nostratech.example.app.vo.PeopleRequestVO;
import com.nostratech.example.app.vo.PeopleResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by yukibuwana on 2/2/17.
 */

@Service
public class PeopleService extends BaseService<People, PeopleResponseVO, PeopleRequestVO> {

    @Autowired
    PeopleRepository peopleRepository;

    @Autowired
    PeopleVoConverter peopleVoConverter;

    @Autowired
    PeopleValidator peopleValidator;

    @Override
    protected BaseRepository<People> getJpaRepository() {
        return peopleRepository;
    }

    @Override
    protected JpaSpecificationExecutor<People> getSpecRepository() {
        return peopleRepository;
    }

    @Override
    protected IBaseVoConverter<PeopleRequestVO, PeopleResponseVO, People> getVoConverter() {
        return peopleVoConverter;
    }

    @Override
    protected PeopleResponseVO add(People people) {

        // validate input
        String messageValidation = peopleValidator.validateAdd(people);
        if (null != messageValidation) throw new BackendExampleException(messageValidation);

        return super.add(people);
    }

    @Override
    protected PeopleResponseVO update(People people, PeopleRequestVO vo) {

        // validate input
        String messageValidation = peopleValidator.validateUpdate(vo);
        if (null != messageValidation) throw new BackendExampleException(messageValidation);

        return super.update(people, vo);
    }

    public Collection<PeopleCustomResponseVO> getAllPeoleByAddress() {
        // init variable
        List<PeopleCustomResponseVO> peopleCustomResponseVOS = new ArrayList<>();

        // get all distinct address
        List<String> addressList = peopleRepository.findAddressDistinct();

        for (String address : addressList) {
            PeopleCustomResponseVO result = new PeopleCustomResponseVO();

            // set address
            result.setAddress(address);

            // find all name base on address
            List<People> nameList = peopleRepository.findByAddress(address);

            List<String> nameResult = new ArrayList<>();
            for (People name : nameList) {
                nameResult.add(name.getName());
            }

            // set name
            result.setName(nameResult);

            // set all value into response
            peopleCustomResponseVOS.add(result);
        }

        return peopleCustomResponseVOS;
    }
}
