package com.nostratech.example.app.enums;

/**
 * Created by yukibuwana on 3/31/17.
 */
public interface BaseModelEnum<T> {

    /**
     * get internal value
     */
    T getInternalValue();
}
