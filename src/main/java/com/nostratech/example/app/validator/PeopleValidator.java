package com.nostratech.example.app.validator;

import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import com.nostratech.example.app.util.ValidationUtil;
import com.nostratech.example.app.vo.PeopleRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by yukibuwana on 4/1/17.
 */

@Component
public class PeopleValidator {

    private String errorFieldRequired;

    private String errorMaxLength;

    private String errorDuplicate;

    @Autowired
    MessageSource messageSource;

    @Autowired
    PeopleRepository peopleRepository;

    private void initErrorMessage() {

        if (errorFieldRequired == null) {
            errorFieldRequired = messageSource.getMessage("error.field.required", null,
                    LocaleContextHolder.getLocale());
        }

        if (errorMaxLength == null) {
            errorMaxLength = messageSource.getMessage("error.max.length", null,
                    LocaleContextHolder.getLocale());
        }

        if (errorDuplicate == null) {
            errorDuplicate = messageSource.getMessage("error.duplicate", null,
                    LocaleContextHolder.getLocale());
        }
    }

    public String validateAdd(People people) {

        initErrorMessage();

        // check required
        if (ValidationUtil.isEmptyOrNull(people.getName())) {
            return String.format(errorFieldRequired, "Name");
        }

        if (ValidationUtil.isEmptyOrNull(people.getAddress())) {
            return String.format(errorFieldRequired, "Address");
        }

        // check length value
        if (ValidationUtil.isNotEmptyOrNull(people.getName()) && people.getName().length() > 20) {
            return String.format(errorMaxLength, "Name", "20");
        }

        if (ValidationUtil.isNotEmptyOrNull(people.getAddress()) && people.getAddress().length() > 50) {
            return String.format(errorMaxLength, "Address", "50");
        }

        // check duplicate
        People find = peopleRepository.findByName(people.getName());
        if (ValidationUtil.isNotEmptyOrNull(find)) {
            return String.format(errorDuplicate, "Name : " + people.getName());
        }

        return null;
    }

    public String validateUpdate(PeopleRequestVO peopleRequestVO) {

        initErrorMessage();

        // check length value
        if (ValidationUtil.isNotEmptyOrNull(peopleRequestVO.getName()) &&
                peopleRequestVO.getName().length() > 6) {
            return String.format(errorMaxLength, "Name", "20");
        }

        if (ValidationUtil.isNotEmptyOrNull(peopleRequestVO.getAddress()) &&
                peopleRequestVO.getAddress().length() > 50) {
            return String.format(errorMaxLength, "Address", "50");
        }

        return null;
    }
}
